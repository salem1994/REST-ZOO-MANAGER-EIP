package eip;
import   org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import  org.apache.camel.builder.RouteBuilder;
import  org.apache.camel.impl.DefaultCamelContext;
import  org.apache.log4j.BasicConfigurator;
import  org.apache.camel.ProducerTemplate;
import org.apache.camel.model.dataformat.JsonLibrary;

import java.util.Scanner;
/**
 * Created by pret on 28/04/2017.
 */
public class ProducerConsumer {
    public static String getMessage(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un message");
        String str = sc.nextLine();
        if (str.equals("exit")) {System.exit(0);}
        return str ;
    }

    public static void main(String[] args) throws Exception {
        String name="";
        String id="";
        String jsonDATA="";
        int mode=0;
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        System.out.println("Veuillez entrer :\n- 1 (Find) \n- 2 (GetALL) \n- 3 (AddAnimal) \n- 4 (DeleteAnimal) \n- 5 (UpdateAnimal) \n- 6 (Service Géonames) \n- 7 (Service Géonames 2) ");
        mode = sc.nextInt();
        if(mode==4|| mode==5) {
            System.out.println("Veuillez entrer l'identifiant d'un animal");
            id = sc2.nextLine();
        }
        if(mode==1 || mode ==6) {
            System.out.println("Veuillez entrer le nom d'un animal");
            name = sc2.nextLine();
        }
        if(mode==3|| mode==5) {

            System.out.println("Veuillez entrer l'objet JSON pour ajouter !");
            jsonDATA = sc2.nextLine();
        }

        // String str = getMessage();
        // Configure le logger par défaut
        BasicConfigurator.configure();
        // Contexte Camel par défaut
        CamelContext context = new DefaultCamelContext();
        // Crée une route contenant le consommateur
        final String finalName = name;
        final String finalId = id;
        RouteBuilder routeBuilder = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                // On définit un consommateur 'consumer-1'
                // qui va écrire le message
                from("direct:consumer-1").to("log:affiche-1-log");
                // On définit un consommateur 'consumer-2'
                // qui va écrire le message
                from("direct:consumer-2").to("file:messages");

                from("direct:consumer-all")
                        .choice()
                        .when(header("destinataire").isEqualTo("ecrire"))
                        .to("direct:consumer-2")
                        .otherwise()
                        .to("direct:consumer-1");

                from("direct:find")
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8090/findbyname/"+ finalName)
                        .log("reponse received : ${body}");

                from("direct:GetALL")
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8090/animals/")
                        .log("reponse received : ${body}");

                from("direct:AddAnimal")
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .setHeader(Exchange.HTTP_METHOD,constant("POST"))
                        .to("http://127.0.0.1:8090/animals/")
                        .log("reponse received : ${body}");

                from("direct:DeleteAnimal")

                        .setHeader(Exchange.HTTP_METHOD,constant("DELETE"))
                        .to("http://127.0.0.1:8090/animals/delete/"+ finalId)
                        .log("reponse received : ${body}");

                from("direct:UpdateAnimal")
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .setHeader(Exchange.HTTP_METHOD,constant("PUT"))
                        .to("http://127.0.0.1:8090/animals/update/"+ finalId)
                        .log("reponse received : ${body}");

                from("direct:geonames")
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8090/cageposition/"+ finalName)
                        .log("reponse received : ${body}");
                from("direct:geonames2")
                        .setHeader(Exchange.HTTP_METHOD,constant("GET"))
                        .to("http://127.0.0.1:8090/centerposition/")
                        .log("reponse received : ${body}");




            }
        };
// On ajoute la route au contexte
        routeBuilder.addRoutesToCamelContext(context);
// On démarre le contexte pour activer les routes
        context.start();
// On crée un producteur
        ProducerTemplate pt = context.createProducerTemplate();
// qui envoie un message au consommateur 'consumer-1'
       /* String head = "";
        if (str.startsWith("w")) {
            head = "ecrire";

        }*/

        // pt.sendBodyAndHeader("direct:consumer-all", str, "destinataire",head);


        switch (mode) {
            case 1:
                pt.sendBody("direct:find", "");
                break;
            case 3:
                pt.sendBody("direct:AddAnimal", jsonDATA);
                break;
            case 2:
                pt.sendBody("direct:GetALL", "");
                break;
            case 4:
                pt.sendBody("direct:DeleteAnimal", "");
                break;
            case 5:
                pt.sendBody("direct:UpdateAnimal", jsonDATA);
                break;
            case 6:
                pt.sendBody("direct:geonames", "");
                break;
            case 7:
                pt.sendBody("direct:geonames2", "");
                break;
            default:pt.sendBody("direct:multiserver", "");
        }


    }
}

